﻿using Common.Logging;
using Common.Logging.Simple;
using JMSAdapterBindingNamespace;

namespace Tru.JNBridge.Async.IntegrationTests
{
	public class MyService : JMSAdapterBindingService
	{
		private readonly ILog _log;

		public MyService()
		{
			LogManager.Adapter = new ConsoleOutLoggerFactoryAdapter(LogLevel.All, true, true, true, "hh:mm:ss");
			_log = LogManager.GetCurrentClassLogger();

			_log.Debug("Starting service");
		}

		public override void OnReceiveTextFromQueue(string name, string text)
		{
			_log.Info(text);
		}
	}
}
