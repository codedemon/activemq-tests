﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using NUnit.Framework;

namespace Tru.JNBridge.Async.IntegrationTests.Tests
{
	[TestFixture]
	public class AsyncTests
	{
		[SetUp]
		public void SetUp()
		{
		}

		[Test]
		public void HandleMessages()
		{
			var baseAddress = new Uri("http://localhost:9010/mq");

			using (var host = new ServiceHost(typeof(MyService), baseAddress))
			{
				var metadata = new ServiceMetadataBehavior
								   {
									   HttpGetEnabled = true,
									   MetadataExporter = { PolicyVersion = PolicyVersion.Policy15 }
								   };

				host.Description.Behaviors.Add(metadata);

				host.Open();

				var e = new AutoResetEvent(false);
				e.WaitOne(TimeSpan.FromMinutes(5));
			}
		}
	}
}
