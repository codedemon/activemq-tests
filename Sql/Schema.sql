-------------------------------------------------------------------------------------------------------------------
-- Database = ActiveMqIntegration
-- Next Version = schema
-- IsHotfix = NO
-------------------------------------------------------------------------------------------------------------------

CREATE DATABASE ActiveMqIntegration;
GO
USE ActiveMqIntegration
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

-------------------------------------------------------------------------------------------------------------------
-- Table: dbo.Numbers 
-------------------------------------------------------------------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Numbers]') AND type in (N'U'))
DROP TABLE [dbo].[Numbers]
GO

CREATE TABLE [dbo].[Numbers] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Data] [int] NOT NULL,
 	CONSTRAINT [PK_Numbers] PRIMARY KEY CLUSTERED (
		[Id] ASC
	)
	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
