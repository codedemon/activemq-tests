﻿using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Common.Logging;
using Common.Logging.Simple;
using MoreLinq;
using NUnit.Framework;
using Tru.ActiveMqTests.Core;

namespace Tru.JNBridge.IntegrationTests.Synchonous
{
	[TestFixture]
	public class PollingDtcTests
	{
		private const int MessagesPerThread = 1000;
		private const string IncomingQueueName = @"dynamicQueues/integration-tests-queue";
		private Repository _repository;
		private ILog _log;

		[SetUp]
		public void SetUp()
		{
			SetUpLogging();
			SetUpRepository();
		}

		[TearDown]
		public void Teardown()
		{

		}

		private void SetUpRepository()
		{
			_repository = new Repository();
			_repository.Clear();
		}

		private static void PopulateMessageQueue(int numberOfMessages)
		{
			var client = new JNBridgeJmsAdapterClient();

			var numbers = Enumerable.Range(1, numberOfMessages);
			var bathedNumbers = numbers.Batch(1000);

			Parallel.ForEach(bathedNumbers, batch =>
												{
													foreach (var value in batch)
													{
														var message = value;
														Task.Factory.StartNew(() => client.SendText(IncomingQueueName, message.ToString(CultureInfo.InvariantCulture)));
													}
												});
		}

		private void SetUpLogging()
		{
			LogManager.Adapter = new ConsoleOutLoggerFactoryAdapter(LogLevel.All, true, false, true, "hh:mm:ss");
			_log = LogManager.GetCurrentClassLogger();
		}

		[TestCase(1)]
		[TestCase(2)]
		public void Can_read_messages_off_the_queue_with_distributed_transactions(int numberOfThreads)
		{
			var totalMessages = numberOfThreads*MessagesPerThread;
			PopulateMessageQueue(totalMessages);
			var reads = new ConcurrentBag<int>();
			var tasks = new Task[numberOfThreads];

			for (var i = 0; i < numberOfThreads; i++)
			{
				tasks[i] = Task.Factory.StartNew(() => ProcessMessages(reads));
			}

			Task.WaitAll(tasks, TimeSpan.FromMinutes(5 * numberOfThreads));

			Assert.That(_repository.Count(), Is.EqualTo(totalMessages), "Number of messages saved");

			var expectedSum = Enumerable.Range(1, totalMessages).Sum();
			Assert.That(_repository.Sum(), Is.EqualTo(expectedSum), "Expected total");

			var totalMessagesProcessed = reads.Count;
			Assert.That(totalMessagesProcessed, Is.GreaterThan(totalMessages), "Messages were put back on the queue and reprocessed");
		}

		private void ProcessMessages(ConcurrentBag<int> reads)
		{
			var client = new JNBridgeJmsAdapterClient();
			var numberOfMessagesProcessed = 0;

			try
			{
				while (true)
				{
					try
					{
						using (var tx = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromSeconds(10)))
						{
							client.Endpoint.Binding.ReceiveTimeout = TimeSpan.FromSeconds(5);

							var document = client.ReceiveText(IncomingQueueName);
							var value = int.Parse(document);
							_repository.Add(value);

							reads.Add(1);

							if (numberOfMessagesProcessed++ % 10 == 0)
								continue;

							tx.Complete();
						}
					}
					catch (TransactionException e)
					{
						_log.Error(e);
					}
				}
			}
			catch (CommunicationException e)
			{
				_log.Info(e);
			}

			_log.InfoFormat("Thread: {0} \t Messages: {1}", Thread.CurrentThread.ManagedThreadId, numberOfMessagesProcessed);
		}
	}
}
