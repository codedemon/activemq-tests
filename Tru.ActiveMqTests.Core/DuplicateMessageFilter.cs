﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Apache.NMS;

namespace Tru.ActiveMqTests.Core
{
	public class DuplicateMessageFilter
	{
		private readonly TimeSpan _windowSize;
		private readonly IDictionary<string, DateTime> _messageIdentifiers;

		public DuplicateMessageFilter(TimeSpan windowSize)
		{
			_windowSize = windowSize;
			_messageIdentifiers = new ConcurrentDictionary<string, DateTime>();
		}

		public bool IsDuplicateMessage(IMessage message)
		{
			var messageId = message.NMSMessageId;

			var messagePresent = _messageIdentifiers.Any(m => m.Key == messageId && m.Value > DateTime.UtcNow - _windowSize);
			return messagePresent;
		}

		public void AddMessage(IMessage message)
		{
			lock (_messageIdentifiers)
			{
				_messageIdentifiers.Add(message.NMSMessageId, DateTime.UtcNow);
			}
		}
	}
}
