﻿using System.Data.Common;
using System.Data.Entity;

namespace Tru.ActiveMqTests.Core.Database
{
	public class DataContext : DbContext
	{
		public IDbSet<Number> Numbers { get; set; }

		public DataContext(string connectionString) : base(connectionString)
		{
		
		}

		public DbConnection Connection()
		{
			return Database.Connection;
		}
	}
}
