﻿using System.ComponentModel.DataAnnotations;

namespace Tru.ActiveMqTests.Core.Database
{
	public class Number
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public int Data { get; set; }
	}
}
