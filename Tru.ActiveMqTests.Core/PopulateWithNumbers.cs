﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.Util;
using Common.Logging;
using MoreLinq;

namespace Tru.ActiveMqTests.Core
{
	public class PopulateWithNumbers
	{
		private readonly IConnectionFactory _connectionFactory;
		private readonly string _destination;
		private readonly ILog _log = LogManager.GetLogger("PopulateNumbers");

		public PopulateWithNumbers(IConnectionFactory connectionFactory, string destination)
		{
			_connectionFactory = connectionFactory;
			_destination = destination;
		}

		public void Populate(int size)
		{
			_log.Debug("Populating MQ with data...");
			var numbers = Enumerable.Range(1, size);
			var batchedNumber = numbers.Batch(1000);

			Parallel.ForEach(batchedNumber, WriteMessages);
			_log.Debug("Populated MQ.");
		}

		private void WriteMessages(IEnumerable<int> numbers)
		{
			using (var connection = _connectionFactory.CreateConnection())
			using (var session = connection.CreateSession())
			using (var producer = session.CreateProducer(SessionUtil.GetDestination(session, _destination)))
			{
				connection.Start();
				_log.DebugFormat("Using concurrent thread with id {0}", Thread.CurrentThread.ManagedThreadId);
				// ReSharper disable once AccessToDisposedClosure
				foreach (var message in numbers.Select(number => producer.CreateTextMessage(number.ToString(CultureInfo.InvariantCulture))))
				{
					producer.Send(message);
				}
			}
		}
	}
}
