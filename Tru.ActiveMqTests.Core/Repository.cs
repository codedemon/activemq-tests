﻿using System.Configuration;
using System.Linq;
using Common.Logging;
using Tru.ActiveMqTests.Core.Database;

namespace Tru.ActiveMqTests.Core
{
	public class Repository
	{
		private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;
		private readonly ILog _log = LogManager.GetLogger("Repository");

		public void Clear()
		{
			_log.Debug("Clearing down database...");
			using (var db = new DataContext(ConnectionString))
			{
				var connection = db.Connection();
				var command = connection.CreateCommand();
				command.CommandText = @"delete from numbers;";
				
				connection.Open();
				command.ExecuteNonQuery();
			}
			_log.Debug("Database cleared.");
		}

		public void Add(int number)
		{
			using (var db = new DataContext(ConnectionString))
			{
				db.Numbers.Add(new Number { Data = number });
				db.SaveChanges();
			}
		}

		public int Count()
		{
			using (var db = new DataContext(ConnectionString))
			{
				return db.Numbers.Count();
			}
		}

		public int Sum()
		{
			using (var db = new DataContext(ConnectionString))
			{
				return db.Numbers.Sum(x => x.Data);
			}
		}
	}
}
