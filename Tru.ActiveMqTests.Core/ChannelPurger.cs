﻿using Apache.NMS;
using Apache.NMS.Util;

namespace Tru.ActiveMqTests.Core
{
	public class ChannelPurger
	{
		private readonly IConnectionFactory _connectionFactory;
		private readonly string _destination;

		public ChannelPurger(IConnectionFactory connectionFactory, string destination)
		{
			_connectionFactory = connectionFactory;
			_destination = destination;
		}

		public void Delete()
		{
			using (var connection = _connectionFactory.CreateConnection())
			using (var session = connection.CreateSession())
			{
				SessionUtil.DeleteDestination(session, _destination);
			}
		}

		public void Purge()
		{
			using (var connection = _connectionFactory.CreateConnection())
			using (var session = connection.CreateSession())
			using ( var consumer = session.CreateConsumer( SessionUtil.GetDestination(session,_destination)))
			{
				connection.Start();
				while (consumer.ReceiveNoWait() != null)
				{
				}
			}
		}
	}
}
