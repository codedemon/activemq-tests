﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.Util;
using NUnit.Framework;

namespace Tru.ActiveMqTests.IntegrationTests.ApacheNMS.SingleQueue.AsyncConsumer
{
	[TestFixture]
	public class AsyncSingleThreadedTests : IntegrationTestsBase
	{
		private const int NumberOfMessages = 2000;
		protected static TimeSpan ReceiveTimeout = TimeSpan.FromSeconds(10);
		private static ConcurrentBag<int> _bag;

		private static readonly AutoResetEvent AllMessagesReadEvent = new AutoResetEvent(false);

		[Test]
		public void Read_the_Messages_with_one_thread()
		{
			PopulateQueue(NumberOfMessages);

			_bag = new ConcurrentBag<int>();

			using (var connection = ConnectionFactory.CreateConnection())
			using (var session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge))
			using (var consumer = session.CreateConsumer(SessionUtil.GetDestination(session, QueueName)))
			{
				connection.Start();
				consumer.Listener += OnMessage;

				Task.Factory.StartNew(() =>
									  {
										  while (MessagesRemainOnQueue())
										  {
											  Thread.Sleep(TimeSpan.FromSeconds(1));
										  }
										  AllMessagesReadEvent.Set();
									  });

				AllMessagesReadEvent.WaitOne(TimeSpan.FromMinutes(2));
			}

			Assert.That(_bag, Has.Count.EqualTo(NumberOfMessages),"Number of messages");
			var expectedTotal = Enumerable.Range(1,NumberOfMessages).Sum();
			Assert.That(_bag.Sum(), Is.EqualTo(expectedTotal));
		}

		private bool MessagesRemainOnQueue()
		{
			using (var connection = ConnectionFactory.CreateConnection())
			using (var session = connection.CreateSession())
			using (var queueBrowser = session.CreateBrowser(SessionUtil.GetQueue(session, QueueName)))
			{
				connection.Start();

				var enumerator = queueBrowser.GetEnumerator();
				if (enumerator.MoveNext())
				{
					return enumerator.Current != null;
				}

				return false;
			}
		}

		protected static void OnMessage(IMessage message)
		{
			var value = int.Parse(((ITextMessage)message).Text);
			_bag.Add(value);
		}
	}
}
