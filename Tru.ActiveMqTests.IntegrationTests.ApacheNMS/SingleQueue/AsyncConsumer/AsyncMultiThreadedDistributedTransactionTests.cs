﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Apache.NMS;
using Apache.NMS.Util;
using Common.Logging;
using NUnit.Framework;
using Tru.ActiveMqTests.Core;

namespace Tru.ActiveMqTests.IntegrationTests.ApacheNMS.SingleQueue.AsyncConsumer
{
	[TestFixture]
	public class AsyncMultiThreadedDistributedTransactionTests : IntegrationTestsBase
	{
		private const int MessagesPerThread = 1000;
		public static ConcurrentBag<int> Reads = new ConcurrentBag<int>();

		[TestCase(1)]
		[TestCase(2)]
		[TestCase(20)]
		public void Read_the_messages_with_multiple_threads_with_dtc(int numberOfThreads)
		{
			var numberOfMessages = MessagesPerThread*numberOfThreads;
			PopulateQueue(numberOfMessages);

			var tasks = new Task[numberOfThreads];
			for (var i = 0; i < numberOfThreads; i++)
			{
				tasks[i] = Task.Factory.StartNew(() =>
					                                 {
						                                 var processor = new MessageProcessor(NetTxConnectionFactory, QueueName, Repository);
						                                 processor.Process();
					                                 });
			}

			Task.WaitAll(tasks);

			Log.InfoFormat("Total reads {0}", Reads.Count);

			Assert.That(Reads.Count, Is.GreaterThanOrEqualTo(numberOfMessages));

			Assert.That(Repository.Count, Is.EqualTo(numberOfMessages), "Number of messages");

			var expected = Enumerable.Range(1, numberOfMessages).Sum();
			Assert.That(Repository.Sum(), Is.EqualTo(expected), "Expected total");

		}

		public class MessageProcessor
		{
			private readonly string _destination;
			private readonly Repository _repository;
			private readonly INetTxConnectionFactory _connectionFactory;
			private static readonly AutoResetEvent AllMessagesReadEvent = new AutoResetEvent(false);
			private static readonly ILog Logger = LogManager.GetLogger("MessageProcessor");

			public MessageProcessor(INetTxConnectionFactory connectionFactory, string destination, Repository repository)
			{
				_connectionFactory = connectionFactory;
				_destination = destination;
				_repository = repository;
			}

			public void Process()
			{
				using (var connection = _connectionFactory.CreateNetTxConnection())
				using (var session = connection.CreateNetTxSession())
				using (var consumer = session.CreateConsumer(SessionUtil.GetDestination(session, _destination)))
				{

					var cc = new CallbackClass(session, _repository);
					consumer.Listener += cc.OnMessage;

					connection.Start();

					Task.Factory.StartNew(() =>
						                      {
							                      while (MessagesRemainOnQueue(_destination))
							                      {
								                      Thread.Sleep(TimeSpan.FromSeconds(2));
							                      }
							                      AllMessagesReadEvent.Set();
						                      });

					AllMessagesReadEvent.WaitOne(TimeSpan.FromMinutes(30));
					Logger.DebugFormat("Thread: {0:000} \t Messages: {1}", Thread.CurrentThread.ManagedThreadId, cc.Count);
				}

			}

			private bool MessagesRemainOnQueue(string queueName)
			{
				using (var connection = _connectionFactory.CreateConnection())
				using (var session = connection.CreateSession())
				using (var queueBrowser = session.CreateBrowser(SessionUtil.GetQueue(session, queueName)))
				{
					connection.Start();

					var enumerator = queueBrowser.GetEnumerator();
					if (enumerator.MoveNext())
					{
						return enumerator.Current != null;
					}

					return false;
				}
			}

			private class CallbackClass
			{
				private readonly INetTxSession _session;
				private readonly Repository _repository;

				public CallbackClass(INetTxSession session, Repository repository)
				{
					_session = session;
					_repository = repository;
					Count = 0;
				}

				public void OnMessage(IMessage message)
				{
					using (var tx = new TransactionScope(TransactionScopeOption.RequiresNew))
					{
						_session.Enlist(Transaction.Current);

						Reads.Add(1);

						var value = int.Parse(((ITextMessage) message).Text);
						_repository.Add(value);

						if (Count++ % 10 != 0)
							tx.Complete();
					}

					Thread.Sleep(TimeSpan.FromMilliseconds(250));
				}

				public int Count { get; private set; }
			}
		}
	}
}