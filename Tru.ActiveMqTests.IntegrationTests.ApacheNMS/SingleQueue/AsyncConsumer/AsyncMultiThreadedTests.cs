﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.Util;
using Common.Logging;
using NUnit.Framework;

namespace Tru.ActiveMqTests.IntegrationTests.ApacheNMS.SingleQueue.AsyncConsumer
{
	[TestFixture]
	public class AsyncMultiThreadedTests : IntegrationTestsBase
	{
		private const int NumberOfThreads = 20;
		private const int NumberOfMessages = 1000*NumberOfThreads;

		[Test]
		public void Read_the_messages_with_multiple_threads()
		{
			var valueBag = new ConcurrentBag<int>();
			PopulateQueue(NumberOfMessages);

			var tasks = new Task[NumberOfThreads];
			for (var i = 0; i < NumberOfThreads; i++)
			{
				tasks[i] = Task.Factory.StartNew(() =>
				                                 {
					                                 var processor = new MessageProcessor(ConnectionFactory, QueueName);
					                                 var values = processor.Process();
					                                 foreach (var value in values)
					                                 {
						                                 valueBag.Add(value);
					                                 }
				                                 });
			}

			Task.WaitAll(tasks);

			Assert.That(valueBag.Count, Is.EqualTo(NumberOfMessages), "Number of messages");

			var expected = Enumerable.Range(1, NumberOfMessages).Sum();
			Assert.That(valueBag.Sum(), Is.EqualTo(expected), "Expected total");

		}

		public class MessageProcessor
		{
			private readonly string _destination;
			private readonly IList<int> _numbers;
			private readonly IConnectionFactory _connectionFactory;
			private static readonly AutoResetEvent AllMessagesReadEvent = new AutoResetEvent(false);
			private static readonly ILog Logger = LogManager.GetLogger("MessageProcessor");

			public MessageProcessor(IConnectionFactory connectionFactory, string destination)
			{
				_connectionFactory = connectionFactory;
				_destination = destination;
				_numbers = new List<int>();
			}

			public int[] Process()
			{
				using (var connection = _connectionFactory.CreateConnection())
				using (var session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge))
				using (var consumer = session.CreateConsumer(SessionUtil.GetDestination(session, _destination)))
				{
					connection.Start();
					consumer.Listener += OnMessage;

					Task.Factory.StartNew(() =>
					                      {
						                      while (MessagesRemainOnQueue(_destination))
						                      {
							                      Thread.Sleep(TimeSpan.FromSeconds(2));
						                      }
						                      AllMessagesReadEvent.Set();
					                      });

					AllMessagesReadEvent.WaitOne(TimeSpan.FromMinutes(2));
				}
				Logger.DebugFormat("Thread: {0:000},\t Reads : {1}", Thread.CurrentThread.ManagedThreadId, _numbers.Count);
				return _numbers.ToArray();
			}

			private bool MessagesRemainOnQueue(string queueName)
			{
				using (var connection = _connectionFactory.CreateConnection())
				using (var session = connection.CreateSession())
				using (var queueBrowser = session.CreateBrowser(SessionUtil.GetQueue(session, queueName)))
				{
					connection.Start();

					var enumerator = queueBrowser.GetEnumerator();
					if (enumerator.MoveNext())
					{
						return enumerator.Current != null;
					}

					return false;
				}
			}

			protected void OnMessage(IMessage message)
			{
				var value = int.Parse(((ITextMessage) message).Text);
				_numbers.Add(value);
			}
		}
	}
}