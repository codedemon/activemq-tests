﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.Util;
using NUnit.Framework;

namespace Tru.ActiveMqTests.IntegrationTests.ApacheNMS.SingleQueue.SynchronousConsumer
{
	[TestFixture]
	public class SyncMultiThreadedTests : IntegrationTestsBase
	{
		private const int NumberOfThreads = 20;
		private const int NumberOfMessages = NumberOfThreads * 1000;

		[Test]
		public void Reads_the_messages_off_the_queue_in_multiple_threads()
		{
			PopulateQueue(NumberOfMessages);
			var bag = new ConcurrentBag<int>();

			ExecuteReadsOnMultipleThreads(bag);

			Assert.That(bag, Has.Count.EqualTo(NumberOfMessages), "Correct number of messages");

			var expectedTotal = Enumerable.Range(1, NumberOfMessages).Sum();
			Assert.That(bag.Sum(), Is.EqualTo(expectedTotal), "Totals match");
		}

		private void ExecuteReadsOnMultipleThreads(ConcurrentBag<int> bag)
		{
			var tasks = new Task[NumberOfThreads];
			for (var i = 0; i < NumberOfThreads; i++)
			{
				tasks[i] = Task.Factory.StartNew(() => ReadMessages(bag));
			}

			Task.WaitAll(tasks);
		}

		private void ReadMessages(ConcurrentBag<int> bag)
		{
			var counter = 0;
			using (var connection = ConnectionFactory.CreateConnection())
			using (var session = connection.CreateSession())
			using (var consumer = session.CreateConsumer(SessionUtil.GetDestination(session, QueueName)))
			{
				connection.Start();

				var thereAreMessagesInTheQueue = true;

				while (thereAreMessagesInTheQueue)
				{
					var message = consumer.Receive(TimeSpan.FromSeconds(5));
					if (message == null)
					{
						thereAreMessagesInTheQueue = false;
						continue;
					}

					var value = int.Parse(((ITextMessage) message).Text);
					bag.Add(value);
					counter++;
				}

				Log.DebugFormat("ThreadId: {0},\tReads: {1}", Thread.CurrentThread.ManagedThreadId, counter);
			}
		}
	}
}
