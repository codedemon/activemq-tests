﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using Apache.NMS;
using Apache.NMS.Util;
using NUnit.Framework;

namespace Tru.ActiveMqTests.IntegrationTests.ApacheNMS.SingleQueue.SynchronousConsumer
{
	[TestFixture]
    public class SyncSingleThreadedTests : IntegrationTestsBase
	{
		private const int NumberOfMessages = 200;

		[Test]
		public void Reads_the_messages_off_the_queue_in_one_thread()
		{
			PopulateQueue(NumberOfMessages);
			var bag = new ConcurrentBag<int>();

			ReadMessages(bag);

			Assert.That(bag, Has.Count.EqualTo(NumberOfMessages), "Correct number of messages");

			var expectedTotal = Enumerable.Range(1, NumberOfMessages).Sum();
			Assert.That(bag.Sum(), Is.EqualTo(expectedTotal),"Totals match");
		}

		private void ReadMessages(ConcurrentBag<int> bag)
		{
			using (var connection = ConnectionFactory.CreateConnection())
			using (var session = connection.CreateSession())
			using (var consumer = session.CreateConsumer(SessionUtil.GetDestination(session, QueueName)))
			{
				connection.Start();

				var thereAreMessagesInTheQueue = true;

				while (thereAreMessagesInTheQueue)
				{
					var message = consumer.Receive(TimeSpan.FromSeconds(5));
					if (message == null)
					{
						thereAreMessagesInTheQueue = false;
						continue;
					}

					var value = int.Parse(((ITextMessage) message).Text);
					bag.Add(value);
				}
			}
		}
	}
}
