﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.Util;
using NUnit.Framework;
using Tru.ActiveMqTests.Core;

namespace Tru.ActiveMqTests.IntegrationTests.ApacheNMS.SingleQueue.SynchronousConsumer
{
	[TestFixture]
	public class SyncMultiThreadedDistributedTransactionalTests : IntegrationTestsBase
	{
		private int _numberOfThreads;
		private const int MessagesPerThread = 1000;
		private DuplicateMessageFilter _duplicateFilter;

		private ConcurrentBag<int> _reads;
		private INetTxConnectionFactory _netTxConnectionFactory;

		[TestCase(1)]
		[TestCase(10)]
		public void Distributed_transaction_reads_the_messages_off_the_queue_in_multiple_threads(int threads)
		{
			_duplicateFilter = new DuplicateMessageFilter(TimeSpan.FromMinutes(5));
			_numberOfThreads = threads;
			var numberOfMessages = _numberOfThreads * MessagesPerThread;

			var mqServer = ConfigurationManager.ConnectionStrings["ActiveMqServer"].ConnectionString;
			_netTxConnectionFactory = new NetTxConnectionFactory(mqServer);

			_reads = new ConcurrentBag<int>();

			PopulateQueue(numberOfMessages);

			ExecuteReadsOnMultipleThreads(_numberOfThreads);

			Log.DebugFormat("Total messages read: {0}", _reads.Count);

			Assert.That(_reads, Has.Count.GreaterThan(numberOfMessages), "Number of reads");

			var databaseRecords = Repository.Count();
			Assert.That(databaseRecords, Is.EqualTo(numberOfMessages), "Number of records");

			var expectedTotal = Enumerable.Range(1, numberOfMessages).Sum();
			Assert.That(Repository.Sum, Is.EqualTo(expectedTotal), "Totals match");
		}

		private void ExecuteReadsOnMultipleThreads(int numberOfThreads)
		{
			var tasks = new Task[numberOfThreads];
			for (var i = 0; i < numberOfThreads; i++)
			{
				tasks[i] = Task.Factory.StartNew(ReadMessages);
			}

			Task.WaitAll(tasks);
		}

		private void ReadMessages()
		{
			var counter = 0;

			using (var connection = _netTxConnectionFactory.CreateNetTxConnection())
			using (var session = connection.CreateNetTxSession())
			using (var consumer = session.CreateConsumer(SessionUtil.GetDestination(session, QueueName)))
			{
				connection.Start();
				connection.RedeliveryPolicy.MaximumRedeliveries = 2;

				var thereAreMessagesInTheQueue = true;

				while (thereAreMessagesInTheQueue)
				{
					try
					{
						using (var tx = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.FromSeconds(10)))
						{
							var message = consumer.Receive(TimeSpan.FromSeconds(2));
							if (message == null)
							{
								thereAreMessagesInTheQueue = false;
								continue;
							}
							_reads.Add(1);

							if (_duplicateFilter.IsDuplicateMessage(message))
								continue;

							var value = int.Parse(((ITextMessage) message).Text);
							Repository.Add(value);

							if (counter++%10 == 0)
								continue;

							_duplicateFilter.AddMessage(message);
							tx.Complete();
						}
					}
					catch (Exception e)
					{
						Log.Error(e);
					}
				}

				Log.DebugFormat("ThreadId: {0},\tReads: {1}", Thread.CurrentThread.ManagedThreadId, counter);
			}
		}
	}
}
