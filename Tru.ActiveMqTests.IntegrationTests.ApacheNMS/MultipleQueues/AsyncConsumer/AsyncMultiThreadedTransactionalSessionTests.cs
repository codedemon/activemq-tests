﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.ActiveMQ.Commands;
using Apache.NMS.Util;
using Common.Logging;
using NUnit.Framework;

namespace Tru.ActiveMqTests.IntegrationTests.ApacheNMS.MultipleQueues.AsyncConsumer
{
	[TestFixture]
	public class AsyncMultiThreadedTransactionalSessionTests : IntegrationTestsBase
	{
		private const int MessagesPerThread = 1000;
		public static ConcurrentBag<int> Reads = new ConcurrentBag<int>();

		[TestCase(1)]
		[TestCase(20)]
		public void Transactionally_read_the_messages_with_multiple_threads(int numberOfThreads)
		{
			var numberOfMessages = numberOfThreads*MessagesPerThread;
			PopulateQueue(numberOfMessages);

			var tasks = new Task[numberOfThreads];
			for (var i = 0; i < numberOfThreads; i++)
			{
				tasks[i] = Task.Factory.StartNew(() =>
				{
					var processor = new MessageProcessor(ConnectionFactory, QueueName, ResultQueueName);
					processor.Process();
				});
			}

			Task.WaitAll(tasks);

			Log.InfoFormat("Total reads {0}", Reads.Count);

			Assert.That(Reads.Count, Is.GreaterThan(numberOfMessages));

			var messagesInResultQueue = MessagesInResultQueue();
			Assert.That(messagesInResultQueue.Count, Is.EqualTo(numberOfMessages), "Result queue message count");

			var expectedSum = Enumerable.Range(1, numberOfMessages).Sum();
			Assert.That(messagesInResultQueue.Sum(), Is.EqualTo(expectedSum));
		}

		private IList<int> MessagesInResultQueue()
		{
			using (var connection = ConnectionFactory.CreateConnection())
			using (var session = connection.CreateSession())
			using (var consumer = session.CreateConsumer(SessionUtil.GetDestination(session, ResultQueueName)))
			{
				connection.Start();

				var result = new List<int>();
				var messagesRemainInQueue = true;
				do
				{
					var message = consumer.Receive(TimeSpan.FromSeconds(5));
					if (message == null)
					{
						messagesRemainInQueue = false;
						continue;
					}

					var value = int.Parse(((ITextMessage)message).Text);
					result.Add(value);
					
				} while (messagesRemainInQueue);

				return result;
			}
		}

		public class MessageProcessor
		{
			private readonly string _sourceQueue;
			private readonly string _resultQueue;
			private readonly IConnectionFactory _connectionFactory;
			private static readonly AutoResetEvent AllMessagesReadEvent = new AutoResetEvent(false);
			private static readonly ILog Logger = LogManager.GetLogger("MessageProcessor");

			public MessageProcessor(IConnectionFactory connectionFactory, string sourceQueue, string resultQueue)
			{
				_connectionFactory = connectionFactory;
				_sourceQueue = sourceQueue;
				_resultQueue = resultQueue;
			}

			public void Process()
			{
				using (var connection = _connectionFactory.CreateConnection())
				using (var session = connection.CreateSession(AcknowledgementMode.Transactional))
				using (var consumer = session.CreateConsumer(SessionUtil.GetDestination(session, _sourceQueue)))
				{
					connection.Start();
					var cc = new CallbackClass(session, _resultQueue);
					consumer.Listener += cc.OnMessage;

					Task.Factory.StartNew(() =>
					{
						while (MessagesRemainOnQueue(_sourceQueue))
						{
							Thread.Sleep(TimeSpan.FromSeconds(2));
						}
						AllMessagesReadEvent.Set();
					});

					AllMessagesReadEvent.WaitOne(TimeSpan.FromMinutes(2));
				}
				Logger.DebugFormat("Thread: {0:000},\t", Thread.CurrentThread.ManagedThreadId);
			}

			private bool MessagesRemainOnQueue(string queueName)
			{
				using (var connection = _connectionFactory.CreateConnection())
				using (var session = connection.CreateSession())
				using (var queueBrowser = session.CreateBrowser(SessionUtil.GetQueue(session, queueName)))
				{
					connection.Start();

					var enumerator = queueBrowser.GetEnumerator();
					if (enumerator.MoveNext())
					{
						return enumerator.Current != null;
					}

					return false;
				}
			}

			private class CallbackClass
			{
				private readonly ISession _session;
				private readonly string _resultQueueName;
				private int _count;

				public CallbackClass(ISession session, string resultQueueName)
				{
					_session = session;
					_resultQueueName = resultQueueName;
					_count = 1;
				}

				public void OnMessage(IMessage message)
				{
					Reads.Add(1);

					var value = int.Parse(((ITextMessage)message).Text);
					using (var producer = _session.CreateProducer(SessionUtil.GetDestination(_session, _resultQueueName)))
					{
						var resultMessage = new ActiveMQTextMessage(value.ToString(CultureInfo.InvariantCulture));
						producer.Send(resultMessage);
					}
					
					if (_count++ % 10 == 0)
					{
						_session.Rollback();
						return;
					}
		
					_session.Commit();
				}
			}
		}
	}
}
