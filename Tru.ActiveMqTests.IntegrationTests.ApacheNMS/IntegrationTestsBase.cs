﻿using System.Configuration;
using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Common.Logging;
using Common.Logging.Simple;
using NUnit.Framework;
using Tru.ActiveMqTests.Core;

namespace Tru.ActiveMqTests.IntegrationTests.ApacheNMS
{
	public class IntegrationTestsBase
	{
		protected IConnectionFactory ConnectionFactory;
		protected INetTxConnectionFactory NetTxConnectionFactory;
		protected string TopicName;
		protected string QueueName;
		protected string ResultQueueName;
		protected static ILog Log;
		protected Repository Repository;

		[SetUp]
		public void SetUp()
		{
			LogManager.Adapter = new ConsoleOutLoggerFactoryAdapter();
			Log = LogManager.GetLogger("IntegrationTest");

			QueueName = ConfigurationManager.AppSettings["QueueDestination"];
			ResultQueueName = ConfigurationManager.AppSettings["QueueDestinationResult"];
			TopicName = ConfigurationManager.AppSettings["TopicDestination"];
			var mqServer = ConfigurationManager.ConnectionStrings["ActiveMqServer"].ConnectionString;
			ConnectionFactory = new NMSConnectionFactory(mqServer);
			NetTxConnectionFactory = new NetTxConnectionFactory(mqServer);
			Repository = new Repository();
			Repository.Clear();
			ClearTopicsAndQueues();
		}

		[TearDown]
		public void TearDown()
		{
			ClearTopicsAndQueues();
		}

		private void ClearTopicsAndQueues()
		{
			new ChannelPurger(ConnectionFactory, QueueName).Delete();
			new ChannelPurger(ConnectionFactory, ResultQueueName).Delete();
			new ChannelPurger(ConnectionFactory, TopicName).Delete();
		}

		protected void PopulateQueue(int numberOfMessages)
		{
			var populateQueue = new PopulateWithNumbers(ConnectionFactory, QueueName);
			populateQueue.Populate(numberOfMessages);
		}
	}
}
